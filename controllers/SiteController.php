<?php

namespace app\controllers;

use yii\data\Pagination;
use app\models\Site;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Coments;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use app\models\SignupForm;
use  yii\data\Sort;
use vova07\imperavi\actions\UploadAction;
use vova07\imperavi\actions\GetAction;

class SiteController extends Controller
{

    public function actionSignup()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'image-upload' => [
                'class' => UploadAction::className(),
                'url' => './library/', // Directory URL address, where files are stored.
                'path' => '@webroot/library/', // Or absolute path to directory where files are stored.
                'uploadOnlyImage' => false,             //чтобы можно было загружать файлы
                'validatorOptions' => ['maxSize' => 400000],    //макс. размер файла
            ],
            'images-get' => [
                'class' => GetAction::className(),
                'url' => './library/', // Directory URL address, where files are stored.
                'path' => '@webroot/library/', // Or absolute path to directory where files are stored.
                'type' => 'GetAction::TYPE_IMAGES',
            ],
            'files-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => './library', // Directory URL address, where files are stored.
                'path' => '@webroot/library/', // Or absolute path to directory where files are stored.
                'type' => '1',//GetAction::TYPE_FILES,
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => './library', // Directory URL address, where files are stored.
                'path' => '@webroot/library/' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new Site();

        $user = "";

        if ($model->load(Yii::$app->request->post())) {
            $model->date = time();

            if($model->validate()) {
                $model->save();

            } else {
                $debugData = Yii::$app->session->get('debugData', []);
                $debugData[] = $model->getErrors();
                Yii::$app->session->set('debugData', $debugData);
            }
        }
        $comments = Site::find()->orderBy('id DESC');

        $pagination = new Pagination([
            'defaultPageSize' =>4,
            'totalCount' =>$comments->count()
        ]);

        $comments = $comments->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $model = new Site();

        return $this->render('index', ['comments' => $comments, 'model' => $model, 'user' => $user, 'pagination' => $pagination]);
    }


    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /*   public function actionAddAdmin() {
        $model = User::find()->where(['username' => 'admin'])->one();
        if (empty($model)) {
            $user = new User();
            $user->username = 'admin';
            $user->email = 'admin@кодер.укр';
            $user->setPassword('admin');
            $user->generateAuthKey();
            if ($user->save()) {
                echo 'good';
            }
        }
    }*/
}
