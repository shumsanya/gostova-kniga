<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Гостьова';

$debugData = Yii::$app->session->get('debugData');
Yii::$app->session->set('debugData', []);
?>

<div>

</div>

<div class="col-md-3 ">
    <div class="forma">
        <p style="font-size: 20px; font-family: 'Century Schoolbook L'; text-align: center; color: tomato">Залиште свою думку

            <?php if(!Yii::$app->user->isGuest) {
                $user = Yii::$app->user->identity->username;
                echo $user; }  ?></p>

        <?php $f = ActiveForm::begin();?>

        <?=Html::submitButton('Опублікувати', ['class' => 'btn btn-success btn-left'])?>

        <?php $model->name = $user;?>
        <?=$f->field($model, 'name')->label('Імя(обов`язково)') ?>

        <?php
        echo $f->field($model, 'text_comment')->widget(Widget::className(), [
            'settings' => [
                'lang' => 'ru',
                'minHeight' => 160,
                'pastePlainText' => true,
                'buttonSource' => true,
                'replaceDivs' => false,
                'plugins' => [
                    'fullscreen',
                    'fontfamily',
                    'fontsize',
                    'fontcolor',
                    'video',
                    'imagemanager',
                ],
            'imageUpload' => Url::to(['/site/image-upload']),
            'imageManagerJson' => Url::to(['/site/images-get']),
            'fileManagerJson' => Url::to(['/site/files-get']),
            'fileUpload' => Url::to(['/site/file-upload'])
            ]
        ]);?>

        <?php ActiveForm::end();?>
    </div>
</div>

<div class="col-md-9">
    <div class="comments">
        <?php foreach($comments as $a):?>
            <div class="comment">
                <b><em style="color: #ff7d44">&nbsp;&bull;&nbsp;автор - <?=$a['name']?>  &nbsp;&bull;&nbsp;опубліковано: <?= date('Y-m-d H:i', $a['date'])?></em></b>
                <div class="comn"><?=$a['text_comment']?></div>
            </div>
        <?php endforeach ?>
    </div>
   <p style="text-align: center"><?=LinkPager::widget(['pagination'=>$pagination])?></p>
</div>


