<?php
/**
 * Created by PhpStorm.
 * User: shum
 * Date: 06.04.17
 * Time: 23:42
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "coments".
 *
 * @property integer $id
 * @property string $name
 * @property string $date
 * @property string $text_comment
 */
class Site extends  \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }


    public function attributeLabels()
    {
        return [
            'name' => 'Імя',
            'date' => 'дата',
            'text_comment' => 'текст',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text_comment', 'date'], 'required'],
            [['name', 'text_comment'], 'string'],
            [['date'], 'integer'],
        ];
    }

}